<head>
<meta charset="UTF-8">
</head>
<h1>Přehled dokumentace</h1>
Přehled dokumentů je dostupný <a href="https://gitlab.fel.cvut.cz/hamsatom/darkovac/wikis/home">zde</a>.<br>
Dokumenty jsou online k zobrazení na <a href="https://sites.google.com/site/sidarkovacoi/">Google sites</a>

<h1>Základní informace</h1>
Dárkovač je webová aplikace pro koordinaci nakupování dárků na určitou událost. Registrovaný uživatel založí novou událost, rozešle pozvánky dalším uživatelům (hostům). Spolu poté vypíší jednotlivé dárky do události (dárek jednotlivě, dárek s podílem mnoha uživatelů).<br>

* <i><b>Název projektu:</b> Dárkovač
* <i><b>Email:</b> darkovac.oi@gmail.com
* <i><b>Stránky:</b> <a href="https://gitlab.fel.cvut.cz/hamsatom/darkovac/wikis/home">wiki</a> nebo <a href="https://sites.google.com/site/sidarkovacoi/">Google sites</a>, kde je dokumentace k zobrazení online 
* <i><b>Autoři:</b> Victoria Eyhkhmann, Tomáš Hamsa, Vojtěch Karen, Timofei Likharev
* <i><b>Cvičení:</b> B161 ZS 2016/2017, paralelka 102 - pondělí 12:45 - 14:15
* <i><b>Cvičící:</b> Ing. Martin Komárek</i>